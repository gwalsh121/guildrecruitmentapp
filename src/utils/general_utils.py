def progress_bar(curr, total, full_progbar):
    percent = round(100 - (curr/total)*100)
    filled_progress_bar = round(full_progbar*percent/100)
    print('\r', '#'*filled_progress_bar + '-'*(full_progbar-filled_progress_bar), str(percent) + '%', end='')
