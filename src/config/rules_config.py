classes = ['warlock','priest','paladin','shaman','monk']
specs = ['holy','discipline','restoration','elemental','shadow','affliction','destruction','demonology','healer','dd','damage']
pure_dps_classes = ['rogue','mage','warlock','hunter',]
faction = ['horde','alliance']
realm = ['silvermoon']
item_level = 410
azerite = 45
battle_of_dazaralor = 6
alliance_races = ['human','dwarf','night elf','gnome','draenei','worgen','pandaren','dark iron dwarf','kul tiran','lightforged draenei','void elf']
horde_races = ['orc','undead','tauren','troll','blood elf','goblin','pandaren','highmountain tauren','mag\'har orc','nightborne','zandalari troll']
looking_for_guild = ['Yes']
difficulty = 'mythic'
experience = {
    'tier_350':0,
    'tier_340':6,
    'tier_330':0
}
last_updated = 7.0 # days
