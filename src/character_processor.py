from config import processor_config
import file_writer

def process_character(character):
    if processor_config.output_type is 'xlsx':
        file_writer.output_to_xlsx(character, 'Potential Recruits')
    elif processor_config.output_type is 'txt':
        file_writer.output_to_txt(character)
