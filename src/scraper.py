import requests
from utils import scraper_utils
from config import scraper_config

from bs4 import BeautifulSoup

base_url = 'https://www.wowprogress.com'

def run_lfg_scraper():
    lfg_url = '/gearscore/eu/char_rating'
    pagination = '/next/'
    filters = '/lfg.1/raids_week.3/lang.en/sortby.ts#char_rating'
    max_page = scraper_config.max_page
    current_page = 0
    total_characters = max_page * 23
    total_characters_running_total = max_page * 23
    response = requests.get(base_url + lfg_url + filters)
    while current_page < max_page:
        soup = BeautifulSoup(response.text, 'html.parser')
        scraper_utils.parse_lfg_soup(soup, total_characters, total_characters_running_total)
        total_characters_running_total -= 23
        response = requests.get(base_url + lfg_url + pagination + str(current_page) + filters)
        current_page += 1

def run_profile_scraper(profile):
    response = requests.get(base_url + profile)
    soup = BeautifulSoup(response.text, 'html.parser')
    return soup
