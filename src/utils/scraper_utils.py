import time
from utils import general_utils

from .character_utils import populate_character_object

def parse_lfg_soup(soup, total_characters, total_characters_running_total):
    for character in soup.find_all('tr'):
        character_attributes = character.find_all('td')
        if len(character_attributes) == 6 and character_attributes[0].find('a', 'character'):
            populate_character_object(character_attributes)
            total_characters_running_total -= 1
            general_utils.progress_bar(total_characters_running_total, total_characters, 100)
            time.sleep(1)
