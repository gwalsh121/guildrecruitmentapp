import datetime
import json

character_headings = ('NAME','BATTLETAG','CLASS','GUILD','FACTION','REALM','SPECS','ITEM LEVEL',\
    'AZERITE','PROFILE','EXPERIENCE','LOOKING FOR GUILD','READY TO TRANSFER','LAST UPDATED')

class Character:
    name = ''
    battletag = ''
    class_ = ''
    guild = ''
    faction = ''
    realm = ''
    specs = ''
    item_level = 0.0
    azerite = 0
    profile = ''
    experience = {
        'tier_350':0,
        'tier_340':0,
        'tier_330':0
    }
    looking_for_guild = False
    ready_to_transfer = False
    last_updated = 0

    def character_to_csv(self):
        return (self.name,self.battletag,self.class_,self.guild,self.faction,self.realm,self.specs,self.item_level,\
            self.azerite,self.profile,json.dumps(self.experience),self.looking_for_guild,self.ready_to_transfer,\
            str(datetime.datetime.fromtimestamp(int(self.last_updated)).strftime('%c')))

    def __str__(self):
        return 'name='+str(self.name)+', battletag='+str(self.battletag)+', class='+str(self.class_)+', guild='+str(self.guild)+', faction='+str(self.faction)+\
            ', realm='+str(self.realm)+', specs='+''.join(self.specs)+', item_level='+str(self.item_level)+', azerite='+str(self.azerite)+', profile='+str(self.profile)+\
            ', experience='+json.dumps(self.experience)+', looking_for_guild='+str(self.looking_for_guild)+', ready_to_transfer='+str(self.ready_to_transfer)+\
            ', last_updated='+str(self.last_updated)
