import rules_engine
from config import rules_config
import scraper
import re

from character import Character

def populate_character_object(character_attributes):
    character_obj = Character()
    character_obj = populate_character_object_from_lfg(character_attributes, character_obj)
    character_obj = populate_character_object_from_profile(character_obj.profile, character_obj)
    rules_engine.run_rules_engine(character_obj)

def populate_character_object_from_lfg(character_attributes, character_obj):
    character_obj.name = get_character_name(character_attributes)
    character_obj.guild = get_character_guild(character_attributes)
    character_obj.realm = get_character_realm(character_attributes)
    character_obj.item_level = get_character_item_level(character_attributes)
    character_obj.profile = get_character_profile(character_attributes)
    return character_obj

def populate_character_object_from_profile(profile, character_obj):
    soup = scraper.run_profile_scraper(profile)
    info_box_divs = get_info_box_divs(soup)
    character_obj.class_ = get_character_class(soup)
    character_obj.battletag = get_character_battletag(info_box_divs)
    character_obj.looking_for_guild = get_character_looking_for_guild(info_box_divs)
    character_obj.ready_to_transfer = get_character_ready_to_transfer(info_box_divs)
    character_obj.specs = get_character_specs(info_box_divs, character_obj.class_)
    character_obj.faction = get_character_faction(soup)
    character_obj.azerite = get_character_azerite(soup)
    character_obj.experience = get_character_experience(soup)
    character_obj.last_updated = get_last_updated(soup)
    return character_obj

def get_info_box_divs(soup):
    info_box_divs = soup.find_all('div', 'language')
    return info_box_divs

def get_character_name(character_attributes):
    character_anchor = character_attributes[0].find('a', 'character')
    if character_anchor:
        return character_anchor.get_text()
    return ''

def get_character_battletag(info_box_divs):
    battletag_span = info_box_divs[0].find('span', 'profileBattletag')
    if battletag_span:
        return battletag_span.text
    return ''

def get_character_profile(character_attributes):
    character_anchor = character_attributes[0].find('a', 'character')
    if character_anchor:
        return character_anchor.attrs['href']
    return ''

def get_character_guild(character_attributes):
    guild_anchor = character_attributes[1].find('a', 'guild')
    if guild_anchor:
        return guild_anchor.get_text()
    else:
        return ''
    return ''

def get_character_realm(character_attributes):
    character_anchor = character_attributes[3].find('a')
    if character_anchor:
        return character_anchor.get_text()
    return ''

def get_character_item_level(character_attributes):
    character_anchor = character_attributes[4]
    if character_anchor:
        if character_anchor.get_text() != '--':
            return float(character_anchor.get_text())
        else: 
            return 0.0
    return 0.0

def get_character_class(soup):
    return soup.find('i').find('span').text

def get_character_faction(soup):
    guild_anchor = soup.find('a', 'guild')
    if guild_anchor:
        guild_anchor_classes = guild_anchor.attrs['class']
        return guild_anchor.attrs['class'][1]
    else:
        race = re.search('^<i>(.*)<span', str(soup.find('i'))).group(1)
        if race in rules_config.alliance_races:
            return 'alliance'
        elif race in rules_config.horde_races:
            return 'horde'
        else:
            return ''

def get_character_looking_for_guild(info_box_divs):
    for div in info_box_divs:
        looking_for_guild_text = div.get_text()
        if 'Looking for guild' in looking_for_guild_text:
            if 'Yes' in looking_for_guild_text:
                return True
            else:
                return False
    return False

def get_character_ready_to_transfer(info_box_divs):
    for div in info_box_divs:
        if 'Looking for guild' in div.get_text() and 'No' not in div.get_text():
            looking_for_guild_span = div.find('span')
            if looking_for_guild_span:
                if 'ready to transfer' in looking_for_guild_span.text:
                    return True
    return False

def get_character_specs(info_box_divs, class_):
    for div in info_box_divs:
        if 'Specs playing' in div.get_text():
            specs_div = div.find('strong')
            if specs_div:
                return specs_div.text
            elif 'unknown' in div.get_text() and class_ in rules_config.pure_dps_classes:
                return 'damage'
    return ''

def get_character_azerite(soup):
    gearscore_divs = soup.find_all('div', 'gearscore')
    for div in gearscore_divs:
        if 'Azerite Level' in div.get_text():
            return int(div.find('span').text)
    return 0

def get_character_experience(soup):
    tiers_div = soup.find('div', id='tiers_details')
    experience = {}
    if tiers_div:
        for tier in tiers_div.find_all('div', 'tier_area'):
            bosses = tier.find_all('span', re.compile('^progress_'))
            kill_count = 0
            for boss in bosses:
                if rules_config.difficulty in boss.get_text().lower():
                    kill_count += 1
            experience[tier.attrs['id']] = kill_count
    return experience

def get_last_updated(soup):
    last_updated_span = soup.find('span', 'datetime')
    if last_updated_span:
        epoch_timestamp = last_updated_span.attrs['data-ts']
        return epoch_timestamp
    else:
        return 0
