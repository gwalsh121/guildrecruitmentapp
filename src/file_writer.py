import os
from config import processor_config
import character as character_class
from utils import file_writer_utils

from pathlib import Path
from openpyxl import Workbook
from openpyxl import load_workbook

def output_to_txt(character):
    output_file = Path(processor_config.output_file.get('txt'))
    output_file.touch(exist_ok=True)
    with open(output_file, 'r+', encoding='utf-8') as file:
        character_found = any(character.battletag in line for line in file)
        if not character_found:
            file.seek(0, os.SEEK_END)
            character_string = character.__str__() + '\n'
            file.write(character_string)

def output_to_xlsx(character, sheet_name):
    workbook_path = processor_config.output_file.get('xlsx')
    workbook = ''
    worksheet = ''
    if os.path.exists(workbook_path):
        workbook = load_workbook(workbook_path)
        worksheet = workbook[sheet_name]
    else:
        workbook = Workbook()
        worksheet = workbook.active
        worksheet.title = sheet_name
        worksheet.append(character_class.character_headings)
    if not file_writer_utils.character_exists_xlsx(character, worksheet):
        worksheet.append(character.character_to_csv())
    workbook.save(workbook_path)
