from config import rules_config as rules
import time
import sys
from character_processor import process_character

def run_rules_engine(character):
    experience_valid = check_experience(character.experience)
    azerite_valid = check_azerite(character.azerite)
    class_valid = check_class(character.class_)
    item_level_valid = check_item_level(character.item_level)
    looking_for_guild_valid = check_looking_for_guild(character.looking_for_guild)
    ready_to_transfer_valid = check_ready_to_transfer(character)
    spec_valid = check_spec(character.specs)
    last_updated_valid = check_last_updated(character.last_updated)

    is_valid = experience_valid\
        and azerite_valid\
        and class_valid\
        and item_level_valid\
        and looking_for_guild_valid\
        and ready_to_transfer_valid\
        and spec_valid\
        and last_updated_valid

    if not last_updated_valid:
        sys.exit()
    elif is_valid:
        process_character(character)

def check_experience(experience):
    rules_experience = rules.experience
    if experience:
        for key in experience:
            if experience.get(key, 0) < rules_experience.get(key, 0):
                return False
        return True
    else:
        return False
        
def check_spec(specs):
    specs_list = specs.split(',')
    for spec in specs_list:
        if spec.strip().lower() in rules.specs:
            return True
    return False

def check_class(class_):
    return class_.lower() in rules.classes

def check_azerite(azerite):
    return azerite >= rules.azerite

def check_item_level(item_level):
    return item_level >= rules.item_level

def check_ready_to_transfer(character):
    if check_looking_for_guild(character):
        if character.ready_to_transfer:
            return True
        elif character.realm.lower() in rules.realm:
            return True
        else:
            return False

def check_looking_for_guild(looking_for_guild):
    return looking_for_guild

def check_last_updated(last_updated):
    return (int(time.time())-int(last_updated))/(60*60*24) <= rules.last_updated
