import openpyxl

def auto_adjust_columns(worksheet):
    for column_cells in worksheet.columns:
        length = max(len(str(cell.value or '')) for cell in column_cells)
        worksheet.column_dimensions[column_cells[0].column].width = length
    return worksheet

def character_exists_xlsx(character, worksheet):
    search_text = character.battletag
    for row in worksheet.iter_rows():
        if row[1].value is not None and search_text in row[1].value:
            return True
    return False